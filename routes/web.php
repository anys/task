<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/task', function () {
    return view('task');
});
Route::get('/task/edit/{id}', function () {
    return view('task');
});
Route::post('/booking', 'BookingController@store');
Route::post('/getStatus', 'BookingController@getRecord');

Route::get('/retrieveBooking', 'BookingController@display');
Route::get('/booking/{id}', 'BookingController@retrieve');
Route::post('/booking/edit/{id}', 'BookingController@update');
Route::post('/booking/delete/{id}', 'BookingController@delete');

