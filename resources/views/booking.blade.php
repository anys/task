@extends('layout.template')
@section('content')
<div class="container" id="app">
    <form action="" class="form-horizontal" role="form">
        <fieldset>
            <div class="form-group">
                <label for="dtp_input2" class="col-md-2 control-label">Date Picking</label>
                <div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy"
                    data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input class="form-control" size="16" type="text" value="" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <input type="hidden" id="dtp_input2" value="" /><br />
            </div>
            <div class="form-group">
                <label for="dtp_input3" class="col-md-2 control-label">Time Picking</label>
                <select v-model="time"  class="form-control">
                    <option value="">Select Time</option>
                    <option v-for="(a, index) in timeArr" :value="a">@{{ a }}</option>
                </select>
                <div v-text="timeType"></div>
            </div>
            <div class="text-right">
                <button type="button" class="btn btn-primary " :disabled="formFlag">Submit</button>
            </div>
        </fieldset>
    </form>
</div>

@endsection