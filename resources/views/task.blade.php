@extends('layout.template')
@section('content')
<div class="container">
    <form action="" class="row form-horizontal" role="form">
        <div class="col-sm-6 col-xs-12 form-add">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" placeholder="John" class="form-control" />

        </div>
        <div class="col-sm-6 col-xs-12  form-add">
            <label for="contact">Contact number</label>
            <input type="text" name="contact" id="contact" placeholder="60125454251" class="form-control" />
        </div>
        <div class="show-details row col-xs-12 hidden">
            <div class="col-sm-6 col-xs-12">
                <label for="_date">Date</label>
                <div id="_date"></div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <label for="_time">Time</label>
                <div id="_time"></div>
            </div>
            <div class="col-xs-12">
                <p>Select the new date and time again.</p>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12  form-add">
            <label class="control-label text-left">Date</label>
            <select id="dataDate" name="date" class="form-control" required>
                <option value="">Select Date</option>
            </select>
        </div>
        <div class="col-sm-6 col-xs-12  form-add">
            <label class="control-label  text-left">Time</label>
            <select class="form-control time" id="dataTime" name="time" required>
                <option value="">Select Time</option>
                <option value="09:00" data-timing="AM">09:00</option>
                <option value="09:30" data-timing="AM">09:30</option>
                <option value="10:00" data-timing="AM">10:00</option>
                <option value="10:30" data-timing="AM">10:30</option>
                <option value="11:00" data-timing="AM">11:00</option>
                <option value="11:30" data-timing="AM">11:30</option>
                <option value="12:00" data-timing="PM">12:00</option>
                <option value="12:30" data-timing="PM">12:30</option>
                <option value="01:00" data-timing="PM">01:00</option>
                <option value="01:30" data-timing="PM">01:30</option>
                <option value="02:00" data-timing="PM">02:00</option>
                <option value="02:30" data-timing="PM">02:30</option>
                <option value="03:00" data-timing="PM">03:00</option>
                <option value="03:30" data-timing="PM">03:30</option>
                <option value="04:00" data-timing="PM">04:00</option>
                <option value="04:30" data-timing="PM">04:30</option>
                <option value="05:00" data-timing="PM">05:00</option>
                <option value="05:30" data-timing="PM">05:30</option>
                <option value="06:00" data-timing="PM">06:00</option>
            </select>
        </div>

        <div style="margin-top: 1rem" class="col-xs-12 text-center">
            <div class="err" id="errMsg"></div>
            <div class="show-details hidden">
                <button type="button" class="btn btn-primary btn-cancel">Cancel</button>
            </div>
            <div class=" form-add">
                <button type="button" class="btn btn-primary btn-submit">Submit</button>
            </div>
        </div>

    </form>
    <section class="form-group hidden thank-you text-center">
        <div class="col-xs-12">
            <h3>Thank you!</h3>
            <p class="msg"> </p>
        </div>
    </section>
</div>

@endsection
