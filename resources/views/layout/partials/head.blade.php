<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>CarSome</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="public/css/app.css">
<link rel="stylesheet" href="public/css/vendors/bootstrap.min.css">
<link rel="stylesheet" href="public/css/vendors/bootstrap-theme.min.css">
<!-- Custom styles for this template -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="public/css/vendors/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="/public/css/task.css">
