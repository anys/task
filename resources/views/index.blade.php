<style>
.centered {
  position: fixed;
  top: 50%;
  left: 50%;
  /* bring your own prefixes */
  transform: translate(-50%, -50%);
}
</style>

<div class="container">
    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSeiGM3_DnZJ3HY8b_KvNwJ8UfW-hZhU0Gx0A&usqp=CAU"
        alt="hello world" class="centered">
</div>