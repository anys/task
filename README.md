# task

Basically this a module of Car Inspection Appointment.
The base of this module is Laravel framework.
This challenge is accessible from here: http://aeshah-ceo.com/task

To run this code. Kindly follow the steps below:
1. Clone this whole module.
2. Get the table information task\database\migrations : run task.sql at your local/host myphpadmin. By copy paste.
3. Need to update the database information accordingly .env.
4. They you may run the page via browser example : http://localhost/task

To display all the record is this path : http://localhost/retrieveBooking
To do update : http://localhost/task/edit/{{id}}
To cancel: just click the cancel button to delete the record.

The assumption is you have localhost/own env to run the code.

