<?php 

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Booking; 
use Response;


class BookingController extends Controller {

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'contact' => 'required',
            'date' => 'required',
            'time' => 'required',
        ]);

        $weekDayLimit = 2;
        $weekSatLimit = 4;
        $weekDayLimitQuery =  Booking::where('day', $request->day)->where('time', $request->time)->count();
        $reachLimit = false;    
        if($request->day == 'sat'){
            if($weekDayLimitQuery === $weekSatLimit){
                $reachLimit = true;
                return response()->json(['errorMsg'=>'This slot is full!', 'errorCode'=>'1001']);
            }
        }else{
            if($weekDayLimitQuery === $weekDayLimit){
                $reachLimit = true;
                return response()->json(['errorMsg'=>'This slot is full!', 'errorCode'=>'1001']);
            }            
        }
        
        if(!$reachLimit){
            $booking = new Booking;
            $booking->name = $request->name;
            $booking->contact = $request->contact;
            $booking->date = $request->date;
            $booking->time = $request->time;
            $booking->day = $request->day;
            $booking->save();
            //return $booking;
            //DB::table('users')->where('id', $id)->update($data);
            return response()->json(['success'=>'Form is successfully submitted!', 'data' => $booking->id]);
        }

    }
    public function getRecord(Request $request)
    {
        $request->validate([
            'date' => 'required',
            'time' => 'required',
        ]);
        $weekDayLimit = 2;
        $weekSatLimit = 4;
        $weekDayLimitQuery =  Booking::where('day', $request->day)->where('time', $request->time)->count();
        $reachLimit = false;    
        if($request->day == 'sat'){
            if($weekDayLimitQuery === $weekSatLimit){
                $reachLimit = true;
                return response()->json(['errorMsg'=>'This slot is full!', 'errorCode'=>'1001']);
            }
        }else{
            if($weekDayLimitQuery === $weekDayLimit){
                $reachLimit = true;
                return response()->json(['errorMsg'=>'This slot is full!', 'errorCode'=>'1001']);
            }            
        }
        if(!$reachLimit){
            return response()->json(null);
        }        
    }
    public function display()
    {
        $yes = Booking::all();
        //$yes =  Booking::where('day', $day)->where('time', $time)->get();
        //return json_encode($yes);
        return response()->json(['bookings'=>$yes]);
    }

    public function retrieve($id)
    {
        $singleVal = Booking::find($id);
        if($singleVal){
            return response()->json(['booking'=>$singleVal]);
        }else {
            return response()->json(['errorMsg'=>'This booking id is not found!', 'errorCode'=>'4001']); 
        }
        
    }

    public function update(Request $request, $id)
    {
        $booking = Booking::find($id);
        $booking->name = $request->name;
        $booking->contact = $request->contact;
        $booking->date = $request->date;
        $booking->time = $request->time;
        $booking->day = $request->day;
        $booking->save();

        return response()->json(['message'=>'Your booking is now update!']);
    }

    public function delete($id)
    {
        $booking = Booking::find($id);
        $booking->delete();

        return response()->json(['message'=>'Your booking is now cancel!']);
    }

}