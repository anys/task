$(function () {
    let flagValidated = false;
    let editFlag = false,
        bookingId;
    init();
    listener();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function init() {
        // const queryString = window.location.search;
        // const urlParams = new URLSearchParams(queryString);
        // const m = urlParams.get('m');
        var url = location.href;
        var array = url.split('/');
        var editMode = array[array.length - 2];
        bookingId = array[array.length - 1];
        if (editMode == "edit") {
            editFlag = true;
            
            axios.get('/booking/' + bookingId).then(function (resp) {
                if (!resp.data.errorCode) {
                    var data = resp.data.booking;
                    $("#name").val(data.name);
                    $("#contact").val(data.contact);
                    var newdateFormat = moment(data.date).format('DD/MM/YYYY');
                    $("#_date").text(newdateFormat);
                    $("#_time").text(data.time);
                    $('.show-details').removeClass('hidden');
                }else {
                    $('.form-add').addClass('hidden');
                    $('#errMsg').text(resp.data.errorMsg)
                }

            }).catch(function (error) {

            });
        }
        var now = moment(moment(), 'hh:mm:ss A');
        var cutOff = moment(moment(), '06:00:00 PM')
        //  if (now.isBefore(cutOff)) {
        for (var i = 0; i < 21; i++) {
            var date = moment().add(i, 'd').format('YYYY-MM-DD');
            var dd = moment().add(i, 'd').format('ddd').toLowerCase();
            if (dd != 'sun') {
                $('#dataDate').append($('<option>', {
                    value: date,
                    text: moment().add(i, 'd').format('DD/MM/YYYY') + ' - ' + moment().add(i, 'd').format('dddd'),
                    'data-day': dd
                }));
            }
        }
        //}

    }

    function listener() {
        let valDate, valTime;

        $("#dataDate").on('change', function () {
            valDate = $(this).val();
            checkStatus();
        })
        $("#dataTime").on('change', function () {
            valTime = $(this).val();
            checkStatus();
        })
        $('.btn-submit').on('click', function (e) {
            e.preventDefault();
            let valName = $('#name').val(),
                valContact = $('#contact').val();
            $(".error").remove();
            $("#errMsg").text('')

            if (valName === "") {
                $('#name').after('<span class="error">This field is required</span>');
            }
            if (valContact === "") {
                $('#contact').after('<span class="error">This field is required</span>');
            }
            if (valDate === undefined) {
                $('#dataDate').after('<span class="error">This field is required</span>');
            }
            if (valTime === undefined) {
                $('#dataTime').after('<span class="error">This field is required</span>');
            }

            if (valName !== "" && valContact !== "" && valDate !== undefined && valTime !== undefined) {
                submit();
            }
        });
        $('.btn-cancel').on('click', function (e) {
            e.preventDefault();
            axios.post('/booking/delete/' + bookingId).then(function (resp) {
                if (!resp.data.errorCode) {
                    $('form').addClass('hidden')
                    $('.thank-you').removeClass('hidden')
                    $('.msg').text(resp.data.message);
                } else {
                    $("#errMsg").text(resp.data.errorMsg)
                }
            }).catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data.message);
                    $("#errMsg").text(error.response.data.message)
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                    $("#errMsg").text(error.request)
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                    $("#errMsg").text(error.message)
                }
            });
        });
    }

    function validateTime(d, t, am) {
        var today = moment().format('YYYY-MM-DD');
        console.log(d, t, am)
        if (d === today) {
            var format = 'hh:mm:ss A'
            var selectedTime = moment(t + ' ' + am, format),
                currTime = moment(moment(), format);
            //afterTime = moment('00:10:00', format);
            var roundDown = moment().startOf('hour');
            roundDown.format('HH:mm A'); // 12:00:00
            var tempTime = roundDown.add(1, 'hour').format('HH:mm A')
            var afterTime = moment(tempTime, format);
            console.log('selected time : ' + selectedTime.format('HH:mm A'))
            console.log('now : ' + currTime.format('HH:mm A'))
            console.log('next hour : ' + afterTime.format('HH:mm A'))
            if (selectedTime.isBetween(currTime, afterTime)) {
                console.log('is between')
                flagValidated = false;
                return $("#errMsg").text('You are not allowed to book an inspection appointment in the current hour.')
            } else if (!currTime.isBefore(moment(selectedTime), "hour")) {
                flagValidated = false;
                return $("#errMsg").text('The time you select is not longer valid.')
            } else {
                console.log('is not between')
                flagValidated = true;

            }
        }
    }


    function checkStatus() {
        $("#errMsg").text('');
        var getTime = $("#dataTime").val();
        var getDate = $("#dataDate").val();

        if (getDate !== "" && getTime !== "") {
            $(".error").remove();
            var AMPM = $("#dataTime").find(':selected').attr('data-timing')
            validateTime(getDate, getTime + ":00", AMPM);
        }

        if (flagValidated) {
            $(".error").remove();
            var data = {
                date: getDate,
                time: getTime,
                day: $("#dataDate").find(':selected').attr('data-day')
            }
            axios.post('/getStatus', data).then(function (resp) {
                $(".error").remove();
                if (!resp.data.errorCode) {
                    //submit();

                    $("#errMsg").text('')
                    return;
                } else {
                    $("#errMsg").text(resp.data.errorMsg)
                }

                console.log(resp.data)
            }).catch(function (error) {
                $(".error").remove();
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data.message);
                    $("#errMsg").text(error.response.data.message)
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                    $("#errMsg").text(error.request)
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                    $("#errMsg").text(error.message.errors)
                }

            });
        }
    }

    function submit() {
        $(".error").remove();
        $("#errMsg").text('');

        var dataPackage = {
            name: $("#name").val(),
            contact: $("#contact").val(),
            date: $("#dataDate").val(),
            time: $("#dataTime").val(),
            day: $("#dataDate").find(':selected').attr('data-day')
        }
        console.log(dataPackage)
        if (editFlag) {
            axios.post('/booking/edit/' + bookingId, dataPackage).then(function (resp) {
                if (!resp.data.errorCode) {
                    $('form').addClass('hidden')
                    $('.thank-you').removeClass('hidden')
                    $('.msg').text(resp.data.message);
                } else {
                    $("#errMsg").text(resp.data.errorMsg)
                }

                console.log(resp.data)
            }).catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data.message);
                    $("#errMsg").text(error.response.data.message)
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                    $("#errMsg").text(error.request)
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                    $("#errMsg").text(error.message)
                }
            });
        } else {
            axios.post('/booking', dataPackage).then(function (resp) {
                if (!resp.data.errorCode) {
                    $('form').addClass('hidden')
                    $('.thank-you').removeClass('hidden')
                    $('.msg').text(resp.data.success);
                } else {
                    $("#errMsg").text(resp.data.errorMsg)
                }

                console.log(resp.data)
            }).catch(function (error) {
                if (error.response) {
                    // Request made and server responded
                    console.log(error.response.data.message);
                    $("#errMsg").text(error.response.data.message)
                } else if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                    $("#errMsg").text(error.request)
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.log('Error', error.message);
                    $("#errMsg").text(error.message)
                }
            });
        }

    }
});
