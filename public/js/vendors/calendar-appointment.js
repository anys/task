$(function () {
    init();
    listener();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function init() {
        for (i = 0; i < 21; i++) {
            var date = moment().add(i, 'd').format('YYYY-MM-DD');
            var dd = moment().add(i, 'd').format('ddd').toLowerCase();
            if(dd != 'sun'){
                $('#dataDate').append($('<option>', { 
                    value: date,
                    text : moment().add(i, 'd').format('DD/MM/YYYY') + ' - ' + dd.toUpperCase(),
                    'data-day' : dd
                }));
            }
        }
    }

    function listener() {
        let dataDate, dataDay, dataTime;
  
        $("#dataDate").on('change', function () {
            dataDate = $(this).val();
            dataDay = $(this).find(':selected').attr('data-day');
        })
        $(".time").on('change', function () {
            dataTime = $(this).val()
        })
        $('.btn').on('click', function (e) {
            e.preventDefault();
            /*if (dateInput === "") {
                alert('Please select date.')
            } else if (timeInput === "") {
                alert('Please select time.')
            } else {
                $.ajax({
                    type: "POST",
                    url: host + '/comment/add',
                    data: $(this).serialize(),
                    success: function (msg) {
                        alert(msg);
                    }
                });
            }*/
            // var mydate = "2017-06-28T00:00:00";

            var dataPackage = {
                date: dataDate,
                time: dataTime,
                day: dataDay
            }
            console.log(dataPackage)
            $.ajax({
                type: "POST",
                url: '/booking',
                data: JSON.stringify(dataPackage),
                'Content-Type': 'application/json',
                success: function (resp) {
                    $('form').addClass('hidden')
                    $('.thank-you').removeClass('hidden')
                    $('.msg').text(resp.success)
                }
            });
        })
    }
});